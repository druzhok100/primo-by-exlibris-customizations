<?php

/*
David Bass
Western Washington University
September 2014
https://developers.exlibrisgroup.com/primo/apis/webservices/xservices/search/briefsearch

PLEASE NOTE: that in order to use the Primo API, your webserver's IP address will need to be granted permission to access the API. 
This can be setup in the Primo Back Office, but for the Orbis Cascade Alliance, it cannot be done at the institution level; it has to be done at the Alliance level (by Al Cornish or the SILS Discovery leader).
 */

$referrer = $_SERVER['HTTP_REFERER'];
$parts = parse_url($referrer);
$domain = $parts['host'];

header('Content-Type: application/javascript');

#TODO: determine if the user is logged-in, and pass that to the curl request


if (($domain == 'onesearch.library.wwu.edu') || ($domain == 'search.library.wwu.edu') || ($domain == 'alliance-primo.hosted.exlibrisgroup.com')) {
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST');
	header('Access-Control-Allow-Headers: EXLRequestType, Origin, Content-Type, Accept');
	header('Access-Control-Request-Headers: x-requested-with');
}

#echo $domain;


if (!isset($_GET['q'])) {
	exit();
} else {
	$q = $_GET['q'];
	$query = "";

	if (is_array($q)) {
		foreach ($q as $value) {
		    $query .= "&query=" . $value;
		}
	} else {
	   $query .= "&query=" . $q;
	}

	$query = str_replace(" ", "+", $query);

}

if (!isset($_GET['s'])) {
	exit();
} else {
	$scope = $_GET['s'];
	$scope = str_replace(" ", "+", $scope);
}


$callback = $_GET['callback'];

if (isset($_GET['callback'])) {
	$callback = $_GET['callback'];
	$callback_no_underscore = str_replace("_", "", $callback);

	# callback should be something like jQuery1830540019340114668_1378922846134
	# to sanitize it, we're going to remove the underscore, and then make sure it's alphanumberic only

	if (!ctype_alnum($callback_no_underscore)) {
		header('status: 400 Bad Request', true, 400);
		exit();
	}

	# TODO: sanitize callback variable - see http://www.geekality.net/2010/06/27/php-how-to-easily-provide-json-and-jsonp/
} else {
	echo "missing callback";
	exit();
}



function getCount($scope, $query) {
	# $url = "http://onesearch.library.wwu.edu/PrimoWebServices/xservice/search/brief?json=true&ip=140.160.178.199&institution=WWU&onCampus=true&indx=1&bulkSize=1&dym=true&lang=eng&" . $scope . "&query=any,contains," . $query;
	$url = "http://onesearch.library.wwu.edu/PrimoWebServices/xservice/search/brief?json=true&ip=140.160.12.179&institution=WWU&onCampus=true&indx=1&bulkSize=1&dym=true&lang=eng&" . $scope . $query;
	//$url = escapeshellcmd($url);

	$curlSession = curl_init();
	curl_setopt($curlSession, CURLOPT_URL, $url);
	curl_setopt($curlSession, CURLOPT_BINARYTRANSFER, true);
	curl_setopt($curlSession, CURLOPT_RETURNTRANSFER, true);
	$data = curl_exec($curlSession);
	curl_close($curlSession);
	$json_array = json_decode($data, true);
	#return $data;
	$count = $json_array["SEGMENTS"]["JAGROOT"]["RESULT"]["DOCSET"]["@TOTALHITS"];
	return number_format($count);
}

if ($scope == "All") {
	/* Books, Articles + More */
	$scope = "loc=local,scope:(NZ)&loc=local,scope:(WWU)&loc=adaptor,primo_central_multiple_fe";
	$num_results["count"] = getCount($scope, $query);
}

if ($scope == "Books") {
	/* At WWU Only */
	$scope = "loc=local,scope:(WWU)";
	$num_results["count"] = getCount($scope, $query);
}

if ($scope == "wwu_summit") {
	/* WWU Libraries + Summit */
	$scope = "loc=local,scope:(NZ)&loc=local,scope:(WWU)";
	$num_results["count"] = getCount($scope, $query);
}

if ($scope == "WorldCat") {
	/* Libraries Worldwide */
	$scope = "loc=adaptor,WorldCatLocal_WWU";
	$num_results["count"] = getCount($scope, $query);
}

// var_dump($num_results);

# convert the match arrays into a json object
$json_response = json_encode($num_results);

# return matches as JSON response
echo $callback . "(" . $json_response . ")";

?>